#include<stdio.h>
#include<math.h>

float input()
{
    float a;
    printf("Value: ");
    scanf("%f",&a);
    return a;
}


float find_distance(float x1,float y1,float x2,float y2)
{
    float distance;
    distance=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    return distance;
}


float output(float x1,float y1,float x2, float y2,float res)
{
    printf("\nDistance between the points (%f,%f) and (%f,%f) = %f\n",x1,y1,x2,y2,res);
}


int main()
{
    float x1,y1,x2,y2,res;
    
    printf("Enter the first co-ordinates (x1,y1)\n");
    x1=input();
    y1=input();
    
    printf("Enter the second co-ordinates (x2,y2)\n");
    x2=input();
    y2=input();
    
    res=find_distance(x1,y1,x2,y2);
    
    output(x1,y1,x2,y2,res);
    return 0;
}


