//Write a program to add two numbers using 4 functions

#include<stdio.h>
int input()
{
    int a;
    printf("Number: \n");
    scanf("%d",&a);
    return a;
}

int sum(int a,int b)
{
    int sum;
    sum=a+b;
    return sum;
}

int output(int x,int y,int res)
{
    printf("Sum of %d and %d = %d\n",x,y,res);
}

int main()
{
    int x,y,res;
    printf("Enter first number");
    x=input();
    printf("Enter second number");
    y=input();
    res=sum(x,y);
    output(x,y,res);
    return 0;
}
